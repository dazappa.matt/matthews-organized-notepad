program writer;

{$mode objfpc}{$H+}

uses
  {$IFDEF UNIX}{$IFDEF UseCThreads}
  cthreads,
  {$ENDIF}{$ENDIF}
  Interfaces, // this includes the LCL widgetset
  Forms, main, resourcestrings, about, settings, wincolors, help, newproject,
  nbitem
  { you can add units after this };

{$R *.res}

begin
  Application.Title:='Matthew''s Organized Notepad';
  RequireDerivedFormResource:=True;
  Application.Initialize;
  Application.CreateForm(TfrmMain, frmMain);
  Application.CreateForm(TfrmAbout, frmAbout);
  Application.CreateForm(TfrmSettings, frmSettings);
  Application.CreateForm(TfrmNewProject, frmNewProject);
  Application.Run;
end.

