unit settings;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, ExtCtrls,
  StdCtrls, ComCtrls, Menus, Spin, MFButton, MFGroupBox, MFListView, IniFiles,
  resourcestrings;

type

  { TfrmSettings }

  TfrmSettings = class(TForm)
    btnChooseBackupLocation: TButton;
    btnEditorBackground: TMFButton;
    btnEditorSeparator: TMFButton;
    btnEditorHighlightText: TMFButton;
    btnEditorHighlight: TMFButton;
    btnNotebookText: TMFButton;
    btnNotebookBackground: TMFButton;
    btnNotebookHighlightText: TMFButton;
    btnNotebookHighlight: TMFButton;
    btnNotebookIcon: TMFButton;
    btnMenuIcon: TMFButton;
    btnToolbarBackground: TMFButton;
    btnSelectFont: TButton;
    btnToolbarIcon: TMFButton;
    btnViewBackups: TButton;
    cbTheme: TComboBox;
    chkAutomaticSaves: TCheckBox;
    chkBackupOnClose: TCheckBox;
    chkDrawBorders: TCheckBox;
    chkEnableSecondaryBackups: TCheckBox;
    chkESRight: TCheckBox;
    chkFullPath: TCheckBox;
    chkHideScrollbars: TCheckBox;
    chkReopenLast: TCheckBox;
    chkSaveRecentProjects: TCheckBox;
    chkUsePercentages: TCheckBox;
    dlgColor: TColorDialog;
    dlgEditorFont: TFontDialog;
    grpBackupNames: TGroupBox;
    grpFont: TGroupBox;
    grpPadding: TGroupBox;
    grpSaving: TGroupBox;
    grpSecondaryBackup: TGroupBox;
    grpStartup: TGroupBox;
    grpVisual: TGroupBox;
    imgSettingsIconsColor: TImageList;
    lblEditorBackgroundColor: TLabel;
    lblEditorHighlight: TLabel;
    lblEditorHighlightText: TLabel;
    lblEditorTextColor: TLabel;
    lblBackupLocation: TLabel;
    lblBottom: TLabel;
    lblFontName: TLabel;
    lblInactivity: TLabel;
    lblLeft: TLabel;
    lblMenuIcon: TLabel;
    lblNotebookBackgroundColor: TLabel;
    lblNotebookHighlight: TLabel;
    lblNotebookHighlightText: TLabel;
    lblNotebookIcon: TLabel;
    lblNotebookTextColor: TLabel;
    lblNumBackups: TLabel;
    lblRight: TLabel;
    lblSecondaryExplanation: TLabel;
    lblSeparator: TLabel;
    lblTheme: TLabel;
    lblToolbarBackground: TLabel;
    lblToolbarHighlight: TLabel;
    lblToolbarIcon: TLabel;
    lblTop: TLabel;
    btnCreateNewTheme: TMFButton;
    btnReloadThemes: TMFButton;
    grpEditor: TMFGroupBox;
    grpNotebook: TMFGroupBox;
    grpMenu: TMFGroupBox;
    grpToolbar: TMFGroupBox;
    btnEditorText: TMFButton;
    lvSettingsCategories: TMFListView;
    mnColor: TPopupMenu;
    mnColorCopy: TMenuItem;
    mnColorPaste: TMenuItem;
    nbSettings: TNotebook;
    pgBackup: TPage;
    pgEditor: TPage;
    pgGeneral: TPage;
    pgHotkeys: TPage;
    pgThemes: TPage;
    rdoDateTime: TRadioButton;
    rdoNumber: TRadioButton;
    seFontSize: TSpinEdit;
    shEditorBackground: TShape;
    shEditorHighlight: TShape;
    shEditorHighlightText: TShape;
    shEditorText: TShape;
    shMenuIcon: TShape;
    shNotebookBackground: TShape;
    shNotebookIcon: TShape;
    shNotebookSelection: TShape;
    shNotebookSelectionBackground: TShape;
    shNotebookText: TShape;
    shSeparator: TShape;
    shToolbarBackground: TShape;
    shToolbarHighlight: TShape;
    shToolbarIcon: TShape;
    shVPSep: TShape;
    imgSettingsIcons: TImageList;
    spPaddingBottom: TSpinEdit;
    spPaddingLeft: TSpinEdit;
    spPaddingRight: TSpinEdit;
    spPaddingTop: TSpinEdit;
    txtBackupLocation: TEdit;
    txtInactivitySeconds: TEdit;
    txtNumBackups: TEdit;
    procedure btnEditorBackgroundClick(Sender: TObject);
    procedure btnEditorSeparatorClick(Sender: TObject);
    procedure btnEditorTextClick(Sender: TObject);
    procedure btnMenuIconClick(Sender: TObject);
    procedure btnNotebookBackgroundClick(Sender: TObject);
    procedure btnNotebookHighlightClick(Sender: TObject);
    procedure btnNotebookHighlightTextClick(Sender: TObject);
    procedure btnNotebookIconClick(Sender: TObject);
    procedure btnNotebookTextClick(Sender: TObject);
    procedure btnReloadThemesMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure btnSelectFontClick(Sender: TObject);
    procedure btnToolbarBackgroundClick(Sender: TObject);
    procedure btnToolbarIconClick(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure cbThemeChange(Sender: TObject);
    procedure chkDrawBordersChange(Sender: TObject);
    procedure chkUsePercentagesChange(Sender: TObject);
    procedure LoadThemeFile();
    procedure FormCreate(Sender: TObject);
    procedure lvSettingsCategoriesSelectItem(SelectedIndex: Integer);
    procedure mnEditorBackgroundCopyClick(Sender: TObject);
    procedure mnEditorBackgroundPasteClick(Sender: TObject);
    procedure mnColorCopyClick(Sender: TObject);
    procedure mnColorPasteClick(Sender: TObject);
    procedure mnColorPopup(Sender: TObject);
    procedure mnMenuIconCopyClick(Sender: TObject);
    procedure mnMenuIconPasteClick(Sender: TObject);
    procedure pgThemesBeforeShow(ASender: TObject; ANewPage: TPage;
      ANewIndex: Integer);
    procedure seFontSizeChange(Sender: TObject);
    procedure shEditorBackgroundMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure shEditorChangeBounds(Sender: TObject);
    procedure shEditorTextMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure shMenuIconMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure shNotebookBackgroundMouseUp(Sender: TObject;
      Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure shNotebookIconMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure shNotebookSelectionBackgroundMouseUp(Sender: TObject;
      Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure shNotebookSelectionMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure shNotebookTextMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure shSeparatorMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure shToolbarBackgroundMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure shToolbarHighlightMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure shToolbarIconMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure setupDefaultTheme();
    function HTMLToPColor(html: string): string;
    procedure spAfterParagraphChange(Sender: TObject);
    procedure spBeforeParagraphChange(Sender: TObject);
    procedure spFirstIndentChange(Sender: TObject);
    procedure spLineSpacingChange(Sender: TObject);
    procedure spPaddingBottomChange(Sender: TObject);
    procedure spPaddingLeftChange(Sender: TObject);
    procedure spPaddingRightChange(Sender: TObject);
    procedure spPaddingTopChange(Sender: TObject);
  private
    { private declarations }
  public
    { public declarations }
  end;

type
  TTheme = record
    name: string;
    editorText: TColor;
    editorBackground: TColor;
    separator: TColor;
    menuIcon: TColor;
    toolbarBackground: TColor;
    toolbarIcon: TColor;
    notebookText: TColor;
    notebookBackground: TColor;
    notebookHighlightText: TColor;
    notebookHighlight: TColor;
    notebookIcon: TColor;
  end;

var
  frmSettings: TfrmSettings;
  needThemeUpdate: boolean = false; // TODO remove
  menuIconUpdate: boolean = false;
  toolbarIconUpdate: boolean = false;
  notebookIconUpdate: boolean = false;
  separatorUpdate: boolean = false;
  borderUpdate: boolean = false;
  notebookUpdate: boolean = false;
  toolbarUpdate: boolean = false;
  editorUpdate: boolean = false;
  paddingUpdate: boolean = false;
  paragraphUpdate: boolean = false;

  haveCopiedColor: boolean = false;
  colorItemPressed: string = '';
  selectedTheme: string = '';
  editorFont: TFont;
  copiedColor: TColor;
  themes: Array of TTheme;

implementation

// https://msdn.microsoft.com/en-us/library/windows/desktop/ms724940(v=vs.85).aspx
// https://msdn.microsoft.com/en-us/library/ms633576.aspx

{$R *.lfm}

// Strings start at 1.
function TfrmSettings.HTMLToPColor(html: string): string;
begin
  result := '$' + html[6] + html[7] + html[4] + html[5] + html[2] + html[3];
end;

procedure TfrmSettings.spAfterParagraphChange(Sender: TObject);
begin
  editorUpdate := true;
end;

procedure TfrmSettings.spBeforeParagraphChange(Sender: TObject);
begin
  editorUpdate := true;
end;

procedure TfrmSettings.spFirstIndentChange(Sender: TObject);
begin
  editorUpdate := true;
end;

procedure TfrmSettings.spLineSpacingChange(Sender: TObject);
begin
  editorUpdate := true;
end;

procedure TfrmSettings.spPaddingBottomChange(Sender: TObject);
begin
  paddingUpdate := true;
end;

procedure TfrmSettings.spPaddingLeftChange(Sender: TObject);
begin
  paddingUpdate := true;
end;

procedure TfrmSettings.spPaddingRightChange(Sender: TObject);
begin
  paddingUpdate := true;
end;

procedure TfrmSettings.spPaddingTopChange(Sender: TObject);
begin
  paddingUpdate := true;
end;

{ TfrmSettings }

procedure TfrmSettings.setupDefaultTheme;
begin
  // TODO
  // OK finding out default system color values is apparently impossible (thanks for nothing)
  // so we can't set the colors of the shapes so we have to hide them.
  // and also: remove the option from the "..." buttons for setting to default color.
end;

procedure TfrmSettings.lvSettingsCategoriesSelectItem(SelectedIndex: Integer);
begin
  // TNotebook is the dumbest component I have ever seen. You cannot rearrange the pages after creation.
  // Pages propety is read only (and also it's blank so who really knows what that is).
  // So now I gotta have some ugly hack here to make the thing function the way I want. Sorry.
  // Do you wish you could nbSettings.PageIndex := Item.Index; ? Cause I do.
    case SelectedIndex of
      0: nbSettings.PageIndex := 0;
      1: nbSettings.PageIndex := 2;
      2: nbSettings.PageIndex := 3;
      3: nbSettings.PageIndex := 4;
      4: nbSettings.PageIndex := 1;
    end;
end;

procedure TfrmSettings.mnEditorBackgroundCopyClick(Sender: TObject);
begin
  copiedColor := shEditorBackground.Brush.Color;
  haveCopiedColor := true;
end;

procedure TfrmSettings.mnEditorBackgroundPasteClick(Sender: TObject);
begin
  shEditorBackground.Brush.Color := copiedColor;
end;

procedure TfrmSettings.mnColorCopyClick(Sender: TObject);
begin
  case colorItemPressed of
    'EditorText': copiedColor := shEditorText.Brush.Color;
    'EditorBackground': copiedColor := shEditorBackground.Brush.Color;
    'Separator': copiedColor := shSeparator.Brush.Color;
    'MenuIcon': copiedColor := shMenuIcon.Brush.Color;
    'ToolbarBackground': copiedColor := shToolbarBackground.Brush.Color;
    'ToolbarIcon': copiedColor := shToolbarIcon.Brush.Color;
    'NotebookText': copiedColor := shNotebookText.Brush.Color;
    'NotebookBackground': copiedColor := shNotebookBackground.Brush.Color;
    'NotebookHighlightText': copiedColor := shNotebookSelection.Brush.Color;
    'NotebookHighlight': copiedColor := shNotebookSelectionBackground.Brush.Color;
    'NotebookIcon': copiedColor := shNotebookIcon.Brush.Color;
  end;
  haveCopiedColor := true;
end;

procedure TfrmSettings.mnColorPasteClick(Sender: TObject);
begin
  case colorItemPressed of
    'EditorText': begin
      shEditorText.Brush.Color := copiedColor;
      editorUpdate := true;
    end;
    'EditorBackground': begin
      shEditorBackground.Brush.Color := copiedColor;
      editorUpdate := true;
    end;
    'Separator': begin
      shSeparator.Brush.Color := copiedColor;
      separatorUpdate := true;
    end;
    'MenuIcon': begin
      shMenuIcon.Brush.Color := copiedColor;
      menuIconUpdate := true;
    end;
    'ToolbarBackground': begin
      shToolbarBackground.Brush.Color := copiedColor;
      toolbarUpdate := true;
    end;
    'ToolbarIcon': begin
      shToolbarIcon.Brush.Color := copiedColor;
      toolbarIconUpdate := true;
    end;
    'NotebookText': begin
      shNotebookText.Brush.Color := copiedColor;
      notebookUpdate := true;
    end;
    'NotebookBackground': begin
      shNotebookBackground.Brush.Color := copiedColor;
      notebookUpdate := true;
    end;
    'NotebookHighlightText': begin
      shNotebookSelection.Brush.Color := copiedColor;
      notebookUpdate := true;
    end;
    'NotebookHighlight': begin
      shNotebookSelectionBackground.Brush.Color := copiedColor;
      notebookUpdate := true;
    end;
    'NotebookIcon': begin
      shNotebookIcon.Brush.Color := copiedColor;
      notebookIconUpdate := true;
    end;
  end;
end;

procedure TfrmSettings.mnColorPopup(Sender: TObject);
begin
  if haveCopiedColor = false then
  begin
    mnColorPaste.Enabled := false;
  end else
  begin
    mnColorPaste.Enabled := true;
  end;
end;

procedure TfrmSettings.mnMenuIconCopyClick(Sender: TObject);
begin
  copiedColor := shMenuIcon.Brush.Color;
  haveCopiedColor := true;
end;

procedure TfrmSettings.mnMenuIconPasteClick(Sender: TObject);
begin
  shMenuIcon.Brush.Color := copiedColor;
end;

procedure TfrmSettings.pgThemesBeforeShow(ASender: TObject; ANewPage: TPage;
  ANewIndex: Integer);
begin

end;

procedure TfrmSettings.seFontSizeChange(Sender: TObject);
begin
  editorUpdate := true;
  editorFont.Size := seFontSize.Value;
end;

procedure TfrmSettings.shEditorBackgroundMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  dlgColor.Color := shEditorBackground.Brush.Color;
  if dlgColor.Execute then
  begin
    editorUpdate := true;
    shEditorBackground.Brush.Color := dlgColor.Color;
  end;
end;

procedure TfrmSettings.shEditorChangeBounds(Sender: TObject);
begin
  end;

procedure TfrmSettings.btnSelectFontClick(Sender: TObject);
begin
  dlgEditorFont.Font := editorFont;
  if dlgEditorFont.Execute then
  begin
    editorFont := dlgEditorFont.Font;
    if editorFont.Name = '' then
    begin
      editorFont.Name := 'Times New Roman';
    end;
    lblFontName.Caption := editorFont.Name;
    seFontSize.Value := editorFont.Size;
    editorUpdate := true;
  end;
end;

procedure TfrmSettings.btnToolbarBackgroundClick(Sender: TObject);
begin
  colorItemPressed := 'ToolbarBackground';
  mnColor.PopUp;
end;

procedure TfrmSettings.btnToolbarIconClick(Sender: TObject);
begin
  colorItemPressed := 'ToolbarIcon';
  mnColor.PopUp;
end;

procedure TfrmSettings.Button1Click(Sender: TObject);
begin
  paragraphUpdate := true;
end;

procedure TfrmSettings.cbThemeChange(Sender: TObject);
begin
  selectedTheme := cbTheme.Items[cbTheme.ItemIndex];
  if cbTheme.ItemIndex = 0 then
  begin

    // Reset colors to OS default
    // Editor
    editorUpdate := true;
    shEditorText.Brush.Color := clDefault;
    shEditorBackground.Brush.Color := clDefault;
    // Separator
    separatorUpdate := true;
    shSeparator.Brush.Color := clForm;
    // Notebook
    notebookUpdate := true;
    notebookIconUpdate := true;
    shNotebookText.Brush.Color := clDefault;
    shNotebookBackground.Brush.Color := clDefault;
    shNotebookSelectionBackground.Brush.Color := clHighlight;
    shNotebookSelection.Brush.Color := clDefault;
    shNotebookIcon.Brush.Color := clBlack;
    // Toolbars
    toolbarUpdate := true;
    toolbarIconUpdate := true;
    shToolbarBackground.Brush.Color := clForm;
    shToolbarIcon.Brush.Color := clDefault;
    // Menu
    menuIconUpdate := true;
    shMenuIcon.Brush.Color := clBlack;
  end
  else
  begin

    menuIconUpdate := true;
    toolbarIconUpdate := true;
    notebookIconUpdate := true;
    separatorUpdate := true;
    borderUpdate := true;
    notebookUpdate := true;
    toolbarUpdate := true;
    editorUpdate := true;

    // Load from the theme
    shEditorText.Brush.Color := themes[cbTheme.ItemIndex-1].editorText;
    shEditorBackground.Brush.Color := themes[cbTheme.ItemIndex-1].editorBackground;
    shSeparator.Brush.Color := themes[cbTheme.ItemIndex-1].separator;
    shMenuIcon.Brush.Color := themes[cbTheme.ItemIndex-1].menuIcon;
    shToolbarBackground.Brush.Color := themes[cbTheme.ItemIndex-1].toolbarBackground;
    shToolbarIcon.Brush.Color := themes[cbTheme.ItemIndex-1].toolbarIcon;
    shNotebookText.Brush.Color := themes[cbTheme.ItemIndex-1].notebookText;
    shNotebookBackground.Brush.Color := themes[cbTheme.ItemIndex-1].notebookBackground;
    //lvSettingsCategories.Color := shNotebookBackground.Brush.Color;
    shNotebookSelection.Brush.Color := themes[cbTheme.ItemIndex-1].notebookHighlightText;
    shNotebookSelectionBackground.Brush.Color := themes[cbTheme.ItemIndex-1].notebookHighlight;
    shNotebookIcon.Brush.Color := themes[cbTheme.ItemIndex-1].notebookIcon;
  end;
end;

procedure TfrmSettings.chkDrawBordersChange(Sender: TObject);
begin
  borderUpdate := true;
end;

procedure TfrmSettings.chkUsePercentagesChange(Sender: TObject);
begin
  paddingUpdate := true;
end;

procedure TfrmSettings.LoadThemeFile();
var
  themeFile: TIniFile;
  themeSections: TStringList;
  i: Integer;
  foundTheme: boolean = false;
begin
  cbTheme.Clear;
  cbTheme.Items.Add(SOSDefaults);
  cbTheme.ItemIndex := 0;
  themeFile := TIniFile.Create('themes.ini');
  themeSections := TStringList.Create;
  try
    themeFile.ReadSections(themeSections);
    cbTheme.Items.AddStrings(themeSections);
    SetLength(themes, themeSections.Count);
    cbTheme.ItemIndex := 0;

    i := 0;
    while i < themeSections.Count do
    begin
      themes[i].name := themeSections[i];
      themes[i].editorText := StringToColor(HTMLToPColor(themeFile.ReadString(themeSections[i], 'editor_text', 'clDefault')));
      themes[i].editorBackground := StringToColor(HTMLToPColor(themeFile.ReadString(themeSections[i], 'editor_background', 'clDefault')));
      themes[i].separator := StringToColor(HTMLToPColor(themeFile.ReadString(themeSections[i], 'separator', 'clDefault')));
      themes[i].menuIcon := StringToColor(HTMLToPColor(themeFile.ReadString(themeSections[i], 'menu_icon', 'clDefault')));
      themes[i].toolbarBackground := StringToColor(HTMLToPColor(themeFile.ReadString(themeSections[i], 'toolbar_background', 'clDefault')));
      themes[i].toolbarIcon := StringToColor(HTMLToPColor(themeFile.ReadString(themeSections[i], 'toolbar_icon', 'clDefault')));
      themes[i].notebookText := StringToColor(HTMLToPColor(themeFile.ReadString(themeSections[i], 'notebook_text', 'clDefault')));
      themes[i].notebookBackground := StringToColor(HTMLToPColor(themeFile.ReadString(themeSections[i], 'notebook_background', 'clDefault')));
      themes[i].notebookHighlightText := StringToColor(HTMLToPColor(themeFile.ReadString(themeSections[i], 'notebook_highlighttext', 'clDefault')));
      themes[i].notebookHighlight := StringToColor(HTMLToPColor(themeFile.ReadString(themeSections[i], 'notebook_highlight', 'clDefault')));
      themes[i].notebookIcon := StringToColor(HTMLToPColor(themeFile.ReadString(themeSections[i], 'notebook_icon', 'clDefault')));
      if selectedTheme = themeSections[i] then
      begin
        cbTheme.ItemIndex := i+1;
        foundTheme := true;
      end;
      Inc(i);
    end;
  finally
    themeFile.Free;
  end;

  cbThemeChange(nil);

end;

procedure TfrmSettings.FormCreate(Sender: TObject);
begin
  {
  {$IFDEF MSWINDOWS}
  lvSettingsCategories.ViewStyle := vsList;
  {$ENDIF}
  {$IFDEF LINUX}
  lvSettingsCategories.ViewStyle := vsIcon;
  {$ENDIF} }

  setupDefaultTheme;
  editorFont := TFont.Create;
  editorFont.Name := 'Times New Roman';
  editorFont.Size := 12;


  lblFontName.Caption := editorFont.Name;
  // todo: doesn't work
  seFontSize.Value := editorFont.Size;

  // todo: load settings

  // Load themes
  LoadThemeFile;
end;

procedure TfrmSettings.btnMenuIconClick(Sender: TObject);
begin
  colorItemPressed := 'MenuIcon';
  mnColor.PopUp;
end;

procedure TfrmSettings.btnNotebookBackgroundClick(Sender: TObject);
begin
  colorItemPressed := 'NotebookBackground';
  mnColor.PopUp;
end;

procedure TfrmSettings.btnNotebookHighlightClick(Sender: TObject);
begin
  colorItemPressed := 'NotebookHighlight';
  mnColor.PopUp;
end;

procedure TfrmSettings.btnNotebookHighlightTextClick(Sender: TObject);
begin
  colorItemPressed := 'NotebookHighlightText';
  mnColor.PopUp;
end;

procedure TfrmSettings.btnNotebookIconClick(Sender: TObject);
begin
  colorItemPressed := 'NotebookIcon';
  mnColor.PopUp;
end;

procedure TfrmSettings.btnNotebookTextClick(Sender: TObject);
begin
  colorItemPressed := 'NotebookText';
  mnColor.PopUp;
end;

procedure TfrmSettings.btnReloadThemesMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  LoadThemeFile;
end;

procedure TfrmSettings.btnEditorTextClick(Sender: TObject);
begin
  colorItemPressed := 'EditorText';
  mnColor.PopUp;
end;

procedure TfrmSettings.btnEditorBackgroundClick(Sender: TObject);
begin
  colorItemPressed := 'EditorBackground';
  mnColor.PopUp;
end;

procedure TfrmSettings.btnEditorSeparatorClick(Sender: TObject);
begin
  colorItemPressed := 'Separator';
  mnColor.PopUp;
end;

procedure TfrmSettings.shEditorTextMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  dlgColor.Color := shEditorText.Brush.Color;
  if dlgColor.Execute then
  begin
    shEditorText.Brush.Color := dlgColor.Color;
    editorUpdate := true;
  end;
end;

procedure TfrmSettings.shMenuIconMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  dlgColor.Color := shMenuIcon.Brush.Color;
  if dlgColor.Execute then
  begin
    shMenuIcon.Brush.Color := dlgColor.Color;
    menuIconUpdate := true;
  end;
end;

procedure TfrmSettings.shNotebookBackgroundMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  dlgColor.Color := shNotebookBackground.Brush.Color;
  if dlgColor.Execute then
  begin
    shNotebookBackground.Brush.Color := dlgColor.Color;
    //lvSettingsCategories.Color := dlgColor.Color;
    notebookUpdate := true;
  end;
end;

procedure TfrmSettings.shNotebookIconMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  dlgColor.Color := shNotebookIcon.Brush.Color;
  if dlgColor.Execute then
  begin
    shNotebookIcon.Brush.Color := dlgColor.Color;
    notebookIconUpdate := true;
  end;
end;

procedure TfrmSettings.shNotebookSelectionBackgroundMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  dlgColor.Color := shNotebookSelectionBackground.Brush.Color;
  if dlgColor.Execute then
  begin
    notebookUpdate := true;
    shNotebookSelectionBackground.Brush.Color := dlgColor.Color;
  end;
end;

procedure TfrmSettings.shNotebookSelectionMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  dlgColor.Color := shNotebookSelection.Brush.Color;
  if dlgColor.Execute then
  begin
    notebookUpdate := true;
    shNotebookSelection.Brush.Color := dlgColor.Color;
  end;
end;

procedure TfrmSettings.shNotebookTextMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  dlgColor.Color := shNotebookText.Brush.Color;
  if dlgColor.Execute then
  begin
    notebookUpdate := true;
    shNotebookText.Brush.Color := dlgColor.Color;
  end;
end;

procedure TfrmSettings.shSeparatorMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  dlgColor.Color := shSeparator.Brush.Color;
  if dlgColor.Execute then
  begin
    separatorUpdate := true;
    shSeparator.Brush.Color := dlgColor.Color;
  end;
end;

procedure TfrmSettings.shToolbarBackgroundMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  dlgColor.Color := shToolbarBackground.Brush.Color;
  if dlgColor.Execute then
  begin
    toolbarUpdate := true;
    shToolbarBackground.Brush.Color := dlgColor.Color;
  end;
end;

procedure TfrmSettings.shToolbarHighlightMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  dlgColor.Color := shToolbarHighlight.Brush.Color;
  if dlgColor.Execute then
  begin
    shToolbarHighlight.Brush.Color := dlgColor.Color;
  end;
end;

procedure TfrmSettings.shToolbarIconMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  dlgColor.Color := shToolbarIcon.Brush.Color;
  if dlgColor.Execute then
  begin
    toolbarIconUpdate := true;
    shToolbarIcon.Brush.Color := dlgColor.Color;
  end;
end;

end.

