unit about;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, StdCtrls,
  ExtCtrls, ComCtrls, resourcestrings, MFLinkLabel;

type

  { TfrmAbout }

  TfrmAbout = class(TForm)
    Label1: TLabel;
    lblThirdParty: TLabel;
    lblLicense: TLabel;
    lblBy: TLabel;
    lblName: TLabel;
    lblVersion: TLabel;
    MFLinkLabel1: TMFLinkLabel;
    moLicense: TMemo;
    moThirdParty: TMemo;
    pgCtrlAbout: TPageControl;
    pnAboutBackground: TPanel;
    tsLicense: TTabSheet;
    tsThirdParty: TTabSheet;
    tsGeneral: TTabSheet;
    procedure FormCreate(Sender: TObject);
  private
    { private declarations }
  public
    { public declarations }
  end;

var
  frmAbout: TfrmAbout;

implementation

{$R *.lfm}

{ TfrmAbout }

procedure TfrmAbout.FormCreate(Sender: TObject);
begin
  lblName.Caption := SSoftwareName;
end;

end.

