# Matthew's Organized Notepad

Matthew's Organized Notepad is a simple text editor with advanced color customization and a sidebar for multiple notes and folders for organization. It does not do anything fancy like spell checking or rich text formatting at this time.

![img](screens/screen1.png?raw=true)

## Dependencies

You must use the Lazarus IDE to compile this program. It requires the [MFComponents](https://github.com/dazappa/mfcomponents) package to be installed. I wrote those components specifically for this program to give me more control over the look and colors of components.

Tested with Lazarus 1.6.4 under Windows 10. It is likely to work cross platform without too much pain. Last I checked, the ListView did not function as expected in the Settings window. Some components may not also work as expected (in particular the PairSplitter did not have its colors changed). I am hoping to write a custom replacement to ensure it works the same across all platforms.
