unit nbitem;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, ComCtrls, Dialogs;

const
  NB_DOCUMENT = 0;
  NB_FOLDER = 1;

type
  TNotebookItem = record
    node: TTreeNode;
    id: Integer; // Unique ID number
    itemType: Integer; // Uses NB_ constants
    saved: boolean; // Whether or not the latest RTF data is saved
    content: string;
    selStart: LongInt; // Selection position aka the caret
    selLength: LongInt; // Selection length (what text is highlighted)
  end;

function IsDocument(n: TTreeNode): boolean;
function GetDocument(n: TTreeNode): Integer;

var
  notebookItems: Array of TNotebookItem;
  displayedIndex: Integer = -1;

implementation

function GetDocument(n: TTreeNode): Integer;
var
  res: Integer = -1;
  i: Integer = 0;
begin
  while i < Length(notebookItems) do
  begin
    if notebookItems[i].node = n then
    begin
      if notebookItems[i].itemType = NB_DOCUMENT then
      begin
        res := i;
      end;
      i := Length(notebookItems);
    end;
    Inc(i);
  end;
  Result := res;
end;

function IsDocument(n: TTreeNode): boolean;
var
  i: Integer = 0;
  found: boolean = false;
begin
  while i < Length(notebookItems) do
  begin
    if notebookItems[i].node = n then
    begin
      if notebookItems[i].itemType = NB_DOCUMENT then
      begin
        found := true;
      end;
      i := Length(notebookItems);
    end;
    Inc(i);
  end;
  Result := found;
end;

end.

