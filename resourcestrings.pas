unit resourcestrings;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, LCLTranslator;

// NOTE TO TRANSLATORS--------------
// See lang/writer.po for all strings
// DO NOT EDIT THIS FILE FOR TRANSLATION

// NOTE TO DEVELOPERS-------------------
// Most strings are automatically pulled from LCL components, so not everything
//  needs to be placed in this file.

resourcestring
  // ---------- MAIN FORM  ----------
  SSoftwareName = 'Matthew''s Organized Notepad';
  SUntitled = 'Untitled';
  // Toolbar
  SHideNotebook = 'Hide Notebook';
  SShowNotebook = 'Show Notebook';
  SNewDocument = 'New Document';
  SNewFolder = 'New Folder';
  SDeleteItem = 'Delete Item';
  // Editor toolbar
  SBold = 'Bold';
  SItalic = 'Italic';
  SUnderline = 'Underline';
  SALeft = 'Align Left';
  SACenter = 'Center';
  SAJustify = 'Justify';
  SARight = 'Align Right';
  // Treeview
  SManuscript = 'Manuscript';
  SNotes = 'Notes';
  SCharacters = 'Characters';
  SPlaces = 'Places';
  STrash = 'Trash';

  // ---------- SETTINGS FORM  ----------
  SOSDefaults = 'Operating System Default';

  // ---------- NEW PROJECT FORM  ----------
  SNewProject = 'New Project';
  SCreateProject = 'Create Project';
  SSaveProjectAs = 'Save Project As';
  SSaveProject = 'Save Project';
  SEnterProjectName = 'Please enter a project name.';
  SSelectProjectPath = 'Please select a location to save the project.';
  SProjectFileExists = 'A project with that name already exists.';
  SErrorCreatingProject = 'There was an error creating the project directory.';
  SKeepDocumentChildren = 'Would you like to keep or delete the sub-documents?';
  SKeepDocChildrenTitle = 'Keep sub-documents?';
  SKeep = 'Keep';
  SDelete = 'Delete';
  SUntitledDocument = 'Untitled Document';
  SUntitledFolder = 'Untitled Folder';

implementation

end.

