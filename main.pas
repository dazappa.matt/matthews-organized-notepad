unit main;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, Menus,
  ComCtrls, ExtCtrls, PairSplitter, toolwin, about, settings, resourcestrings,
  intfgraphics, StdCtrls, fpimage, lclintf, RichMemo, RichMemoUtils, newproject, nbitem,
  MFButton, MFGroupBox;

type

  { TfrmMain }

  TfrmMain = class(TForm)
    imgNotebook: TImageList;
    imgMenuColor: TImageList;
    imgMenu: TImageList;
    imgToolbar: TImageList;
    imgNotebookColor: TImageList;
    imgToolbarColor: TImageList;
    moOne: TMemo;
    miFToolbar: TMenuItem;
    miStatusBar: TMenuItem;
    miToolbar: TMenuItem;
    dlgOpenF: TOpenDialog;
    pnEditorCover: TPanel;
    pnNotebook: TPanel;
    dlgSave: TSaveDialog;
    dlgSelectDir: TSelectDirectoryDialog;
    miNewFolder: TMenuItem;
    miNewDocument: TMenuItem;
    miFileSep3: TMenuItem;
    miSaveAs: TMenuItem;
    miSave: TMenuItem;
    miSettings: TMenuItem;
    miTools: TMenuItem;
    miFullscreen: TMenuItem;
    miNotebook: TMenuItem;
    miPaste: TMenuItem;
    miCopy: TMenuItem;
    miCut: TMenuItem;
    miEditSep1: TMenuItem;
    miRedo: TMenuItem;
    miUndo: TMenuItem;
    miQuit: TMenuItem;
    miFileSep2: TMenuItem;
    miCompile: TMenuItem;
    miFileSep1: TMenuItem;
    miRecent: TMenuItem;
    miOpen: TMenuItem;
    miNew: TMenuItem;
    miAbout: TMenuItem;
    miManual: TMenuItem;
    miHelp: TMenuItem;
    miProject: TMenuItem;
    miView: TMenuItem;
    miEdit: TMenuItem;
    miFile: TMenuItem;
    mMain: TMainMenu;
    pmnNotebook: TPopupMenu;
    psMain: TPairSplitter;
    psLeftSide: TPairSplitterSide;
    psRightSide: TPairSplitterSide;
    sbMain: TStatusBar;
    tbMain: TToolBar;
    tbtnNotebook: TToolButton;
    tbtnNewDocument: TToolButton;
    tbtnNewFolder: TToolButton;
    tbtnDelete: TToolButton;
    tbtnBold: TToolButton;
    tbtnItalic: TToolButton;
    tbtnUnderline: TToolButton;
    tbtnSep1: TToolButton;
    tbtnALeft: TToolButton;
    tbtnACenter: TToolButton;
    tbtnAJustify: TToolButton;
    tbtnARight: TToolButton;
    tmrTheme: TTimer;
    tvNotebook: TTreeView;
    procedure FormCreate(Sender: TObject);
    procedure miAboutClick(Sender: TObject);
    procedure miFullscreenClick(Sender: TObject);
    procedure miManualClick(Sender: TObject);
    procedure miNewClick(Sender: TObject);
    procedure miNewDocumentClick(Sender: TObject);
    procedure miNewFolderClick(Sender: TObject);
    procedure miNotebookClick(Sender: TObject);
    procedure miOpenClick(Sender: TObject);
    procedure miRedoClick(Sender: TObject);
    procedure miSaveAsClick(Sender: TObject);
    procedure miSaveClick(Sender: TObject);
    procedure miSettingsClick(Sender: TObject);
    procedure miStatusBarClick(Sender: TObject);
    procedure miToolbarClick(Sender: TObject);
    procedure miUndoClick(Sender: TObject);
    procedure miFToolbarClick(Sender: TObject);
    procedure psLeftSideMouseEnter(Sender: TObject);
    procedure rmMainMouseEnter(Sender: TObject);
    procedure rmMainMouseLeave(Sender: TObject);
    procedure tmrThemeTimer(Sender: TObject);
    procedure btnDelete(Sender: TObject);
    procedure btnAddDocument(Sender: TObject);
    procedure btnAddFolder(Sender: TObject);
    procedure tvNotebookCustomDrawItem(Sender: TCustomTreeView;
      Node: TTreeNode; State: TCustomDrawState; var DefaultDraw: Boolean);
    procedure disableBorders();
    procedure enableBorders();
    procedure SetActiveDocument(docIndex: Integer);
    procedure AddDocument();
    procedure AddFolder();
    procedure SetButtonColors(tb: TMFButton);
    procedure SetGroupColors(tg: TMFGroupBox);
    procedure tvNotebookMouseEnter(Sender: TObject);
    procedure tvNotebookMouseLeave(Sender: TObject);
    procedure UpdatePadding();
    procedure tvNotebookEditing(Sender: TObject; Node: TTreeNode;
      var AllowEdit: Boolean);
    procedure tvNotebookSelectionChanged(Sender: TObject);
  private
    { private declarations }
  public
    { public declarations }
  end;

var
  frmMain: TfrmMain;
  projectPath: string = '';
  projectName: string = SUntitled;
  globalItemId: Integer = 0;

implementation

{$R *.lfm}

// HERE 1
procedure TfrmMain.SetButtonColors(tb: TMFButton);
begin
  // Dark color base
  tb.ColorFont := moOne.Font.Color;
  tb.ColorBorder := moOne.Font.Color;
  tb.ColorBackground := frmSettings.shEditorBackground.Brush.Color;

  tb.ColorFontHover := tb.ColorBackground;
  tb.ColorBorderHover := moOne.Font.Color;
  tb.ColorBackgroundHover := moOne.Font.Color;

  tb.ColorFontPressed := tb.ColorFontHover;
  tb.ColorBorderPressed := tb.ColorBorderHover;
  tb.ColorBackgroundPressed := tb.ColorBackgroundHover;

  // Light color base
  {tb.Color := frmSettings.shEditorBackground.Brush.Color;
  tb.BorderColor := moOne.Font.Color;
  tb.BackgroundColor := moOne.Font.Color;

  tb.ColorHover := moOne.Font.Color;
  tb.BorderColorHover := moOne.Font.Color;
  tb.BackgroundColorHover := frmSettings.shEditorBackground.Brush.Color;

  tb.ColorPressed := tb.ColorHover;
  tb.BorderColorPressed := tb.BorderColorHover;
  tb.BackgroundColorPressed := tb.BackgroundColorHover;}
end;

procedure TfrmMain.SetGroupColors(tg: TMFGroupBox);
begin
  tg.ColorFont := moOne.Font.Color;
  tg.ColorBorder := moOne.Font.Color;
  tg.ColorBackground := frmSettings.shEditorBackground.Brush.Color;
end;

{ TfrmMain }

// TODO: make UI completely unusable until user loads or creates a new project

procedure TfrmMain.FormCreate(Sender: TObject);
var
  i: integer;
begin
  // TODO: Allow the main nodes to be hidden. Load that setting and hide them here.
  // TODO: Figure out a way to make certain nodes read only. Only TreeView globally supports it.
  // - It might be necessary to set the TreeView RO and then listen for double click and manually
  // - enter editing mode.
  //tvNotebook.Items[0].Data := @asdf;
  for i := 0 to 4 do
  begin
    //tvNotebook.Items[i].EditText;
  end;

  // Raises an error.
  //tvNotebook.Items[0].EditText;

  // TODO: Load toolbar layout for tbMain and apply it here (don't forget hints)
  // Toggle notebook visibility
  tbtnNotebook := TToolButton.Create(tbMain);
  tbtnNotebook.ImageIndex := 0;
  tbtnNotebook.Left := 0;
  tbtnNotebook.Name := 'tbtnNotebook';
  tbtnNotebook.Hint := SHideNotebook;
  tbtnNotebook.ShowHint := true;
  tbtnNotebook.Style := tbsCheck;
  tbtnNotebook.Down := true;
  tbtnNotebook.OnClick := @miNotebookClick;
  tbtnNotebook.Parent := tbMain;
  // New document
  tbtnNewDocument := TToolButton.Create(tbMain);
  tbtnNewDocument.ImageIndex := 1;
  tbtnNewDocument.Left := 1;
  tbtnNewDocument.Hint := SNewDocument;
  tbtnNewDocument.ShowHint := true;
  tbtnNewDocument.Name := 'tbtnNewDocument';
  tbtnNewDocument.OnClick := @btnAddDocument;
  tbtnNewDocument.Parent := tbMain;
  // New folder
  tbtnNewFolder := TToolButton.Create(tbMain);
  tbtnNewFolder.ImageIndex := 2;
  tbtnNewFolder.Left := 2;
  tbtnNewFolder.Name := 'tbtnNewFolder';
  tbtnNewFolder.Hint := SNewFolder;
  tbtnNewFolder.ShowHint := true;
  tbtnNewFolder.OnClick := @btnAddFolder;
  tbtnNewFolder.Parent := tbMain;
  // Delete item
  tbtnDelete := TToolButton.Create(tbMain);
  tbtnDelete.ImageIndex := 3;
  tbtnDelete.Left := 3;
  tbtnDelete.Hint := SDeleteItem;
  tbtnDelete.ShowHint := true;
  tbtnDelete.Name := 'tbtnDeleteItem';
  tbtnDelete.OnClick := @btnDelete;
  tbtnDelete.Parent := tbMain;

  // TODO look into the TRichMemo component to see how to do custom text highlighting colors if possible
  // and also it doesn't use the Windows default colors for this which is odd.
end;

procedure TfrmMain.disableBorders();
begin
  tvNotebook.BorderStyle := bsNone;
  moOne.BorderStyle := bsNone;
  pnNotebook.BorderStyle := bsNone;
end;

procedure TfrmMain.enableBorders();
begin
  tvNotebook.BorderStyle := bsSingle;
  moOne.BorderStyle := bsSingle;
  pnNotebook.BorderStyle := bsSingle;
end;

procedure TfrmMain.miAboutClick(Sender: TObject);
begin
  frmAbout.Show;
end;

procedure TfrmMain.miFullscreenClick(Sender: TObject);
begin
  frmMain.Width := 1200;
  frmMain.Height := 600;
end;

procedure TfrmMain.miManualClick(Sender: TObject);
begin
  //frmHelp.Show;
end;

procedure TfrmMain.miNewClick(Sender: TObject);
begin
  frmNewProject.setCreateProject();
  frmNewProject.Show;
end;

procedure TfrmMain.miNewDocumentClick(Sender: TObject);
begin
  AddDocument;
end;

procedure TfrmMain.miNewFolderClick(Sender: TObject);
begin
  AddFolder;
end;

procedure TfrmMain.miNotebookClick(Sender: TObject);
begin
  // TODO: Make the menu a checkbutton also. And figure out a way to get the programmitically
  // created tbtn to change its checked status
  // See if I can hide the splitter component as well.
  if psLeftSide.Visible then
  begin
    psLeftSide.Hide;
    miNotebook.Checked := false;
    tbtnNotebook.Down := false;
    tbtnNotebook.Hint := SShowNotebook;
  end else
  begin
    psLeftSide.Show;
    miNotebook.Checked := true;
    tbtnNotebook.Down := true;
    tbtnNotebook.Hint := SHideNotebook;
  end;
end;

procedure TfrmMain.miOpenClick(Sender: TObject);
begin
  if dlgOpenF.Execute then
  begin

  end;
end;

procedure TfrmMain.miRedoClick(Sender: TObject);
begin
  //enableBorders;
end;

procedure TfrmMain.miSaveAsClick(Sender: TObject);
begin
  frmNewProject.setSaveAs;
  frmNewProject.Show;
end;

procedure TfrmMain.miSaveClick(Sender: TObject);
begin
  // projectName is blank if we haven't loaded or created a new project
  // Instead of saving, we need to Save As
  if projectPath = '' then
  begin
    frmNewProject.setSaveAs;
    frmNewProject.Show;
    exit;
  end;
end;

procedure DeleteNotebookItem;
const
  mrKeep = 20;
  mrDelete = 21;
var
  curNode: TTreeNode;
begin
  // Make sure there is an item selected
  if not(frmMain.tvNotebook.Selected = nil) then
  begin
    // Make sure the selected item is not one of the parent nodes
    if not(frmMain.tvNotebook.Selected.Parent = nil) then
    begin
      if frmMain.tvNotebook.Selected.HasChildren then
      begin
        case QuestionDlg(SKeepDocChildrenTitle, SKeepDocumentChildren, mtConfirmation, [mrKeep,SKeep,mrDelete,SDelete], 0) of
          mrKeep: begin
            curNode := frmMain.tvNotebook.Selected.GetFirstChild;
            while curNode <> nil do
            begin
              curNode.MoveTo(frmMain.tvNotebook.Selected.Parent, naAddChild);
              curNode := frmMain.tvNotebook.Selected.GetFirstChild;
            end;
            // todo file ops
            frmMain.tvNotebook.Selected.MoveTo(frmMain.tvNotebook.Items.GetLastNode, naAddChild);
            //frmMain.tvNotebook.Items.Delete(frmMain.tvNotebook.Selected);
          end;
          mrDelete: begin
            // TODO file ops
            frmMain.tvNotebook.Selected.MoveTo(frmMain.tvNotebook.Items.GetLastNode, naAddChild);
            //frmMain.tvNotebook.Items.Delete(frmMain.tvNotebook.Selected);
          end;
        end;
      end else
      begin
        // todo file ops
        frmMain.tvNotebook.Selected.MoveTo(frmMain.tvNotebook.Items.GetLastNode, naAddChild); //!here
        //frmMain.tvNotebook.Items.Delete(frmMain.tvNotebook.Selected);
      end;
    end;
  end;
end;

procedure TfrmMain.btnDelete(Sender: TObject);
begin
  DeleteNotebookItem;
end;

procedure TfrmMain.AddDocument;
var
  tmpNode: TTreeNode;
  nl: Integer;
begin
  if not(frmMain.psLeftSide.Visible) then
  begin
    frmMain.psLeftSide.Show;
    frmMain.miNotebook.Checked := true;
    frmMain.tbtnNotebook.Down := true;
    frmMain.tbtnNotebook.Hint := SHideNotebook;
  end;

  // If there is an item selected
  if not(frmMain.tvNotebook.Selected = nil) then
  begin
    // If item is a document, put it in the same level, not sub-level
    if(IsDocument(frmMain.tvNotebook.Selected)) then
    begin
      tmpNode := tvNotebook.Items.AddChild(tvNotebook.Selected.Parent, SUntitledDocument);
      tmpNode.ImageIndex := 7;
      tmpNode.SelectedIndex := 7;
    end else
    begin
      tmpNode := tvNotebook.Items.AddChild(tvNotebook.Selected, SUntitledDocument);
      tmpNode.ImageIndex := 7;
      tmpNode.SelectedIndex := 7;
      tvNotebook.Selected.Expand(false);
    end;
  end else
  begin
    // Add to Manuscript node
    tmpNode := tvNotebook.Items.AddChild(tvNotebook.Items[0], SUntitledDocument);
    tmpNode.ImageIndex := 7;
    tmpNode.SelectedIndex := 7;
    tvNotebook.Items[0].Expand(false);
  end;
  nl := Length(notebookItems);
  SetLength(notebookItems, nl+1);
  Inc(globalItemId);
  notebookItems[nl].id := globalItemId;
  notebookItems[nl].itemType := NB_DOCUMENT;
  notebookItems[nl].node := tmpNode;
  notebookItems[nl].saved := false;
  notebookItems[nl].selStart := 0;
  //notebookItems[nl].content.Create;

  // Change active document
  //docIndex := GetDocument(tvNotebook.Selected);
  SetActiveDocument(nl);

  // Automatically edit the name
  tmpNode.EditText;
end;

procedure TfrmMain.AddFolder;
var
  tmpNode: TTreeNode;
  nl: Integer;
begin
  if not(frmMain.psLeftSide.Visible) then
  begin
    frmMain.psLeftSide.Show;
    frmMain.miNotebook.Checked := true;
    frmMain.tbtnNotebook.Down := true;
    frmMain.tbtnNotebook.Hint := SHideNotebook;
  end;

  // TODO figure out how to do different images when a TTreeNode is expanded

  // If there is an item selected
  if not(frmMain.tvNotebook.Selected = nil) then
  begin
    tmpNode := tvNotebook.Items.AddChild(tvNotebook.Selected, SUntitledFolder);
    tmpNode.ImageIndex := 5;
    tmpNode.SelectedIndex := 5;
    tvNotebook.Selected.Expand(false);
  end else
  begin
    // Add to Manuscript node
    tmpNode := tvNotebook.Items.AddChild(tvNotebook.Items[0], SUntitledFolder);
    tmpNode.ImageIndex := 5;
    tmpNode.SelectedIndex := 5;
    tvNotebook.Items[0].Expand(false);
  end;

  nl := Length(notebookItems);
  SetLength(notebookItems, nl+1);
  Inc(globalItemId);
  notebookItems[nl].id := globalItemId;
  notebookItems[nl].itemType := NB_FOLDER;
  notebookItems[nl].node := tmpNode;
  notebookItems[nl].saved := false;
  notebookItems[nl].selStart := 0;
  // Automatically edit the name
  tmpNode.EditText;
end;

procedure TfrmMain.tvNotebookMouseEnter(Sender: TObject);
begin
  if frmSettings.chkHideScrollbars.Checked then
  begin
    tvNotebook.ScrollBars := ssAutoBoth;
  end;
end;

procedure TfrmMain.tvNotebookMouseLeave(Sender: TObject);
begin
  if frmSettings.chkHideScrollbars.Checked then
  begin
    tvNotebook.ScrollBars := ssNone;
  end;
end;

procedure TfrmMain.tvNotebookEditing(Sender: TObject; Node: TTreeNode;
  var AllowEdit: Boolean);
begin
  // Disallow editing of root nodes
  if Node.Level = 0 then
  begin
    AllowEdit := false;
  end;
end;

procedure TfrmMain.tvNotebookSelectionChanged(Sender: TObject);
var
  docIndex: Integer;
begin
  // If a node is selected and it's a document
  docIndex := GetDocument(tvNotebook.Selected);
  if docIndex > -1 then
  begin
    if moOne.Visible then
    begin
      SetActiveDocument(docIndex);
      // Needed every time so the user can get to quickly typing
      moOne.SetFocus;
    end;
  end;
end;

procedure TfrmMain.btnAddDocument(Sender: TObject);
begin
  AddDocument;
end;

procedure TfrmMain.btnAddFolder(Sender: TObject);
begin
  AddFolder;
end;

procedure TfrmMain.miSettingsClick(Sender: TObject);
begin
  frmSettings.Show;
end;

procedure TfrmMain.miStatusBarClick(Sender: TObject);
begin
  sbMain.Visible := not sbMain.Visible;
  miStatusBar.Checked := not miStatusBar.Checked;
end;

procedure TfrmMain.miToolbarClick(Sender: TObject);
begin
  tbMain.Visible := not tbMain.Visible;
  miToolbar.Checked := not miToolbar.Checked;
end;

procedure TfrmMain.miUndoClick(Sender: TObject);
begin
  //disableBorders;
end;

procedure TfrmMain.UpdatePadding();
var
  w,h: Integer;
begin
  if frmSettings.chkUsePercentages.Checked then
  begin
    w := psRightSide.Width;
    h := psRightSide.Height;
    //pnPaddingTop.Height := Round(h*(frmSettings.spPaddingTop.Value/100));
    //pnPaddingBottom.Height := Round(h*(frmSettings.spPaddingBottom.Value/100));
    //pnPaddingLeft.Width := Round(w*(frmSettings.spPaddingLeft.Value/100));
    //pnPaddingRight.Width := Round(w*(frmSettings.spPaddingRight.Value/100));
  end;
end;

procedure TfrmMain.miFToolbarClick(Sender: TObject);
begin
  miFToolbar.Checked := not(miFToolbar.Checked);
  pnNotebook.Visible := not(pnNotebook.Visible);
end;

procedure TfrmMain.psLeftSideMouseEnter(Sender: TObject);
begin
  //ShowMessage('E');
end;

procedure TfrmMain.rmMainMouseEnter(Sender: TObject);
begin
end;

procedure TfrmMain.rmMainMouseLeave(Sender: TObject);
begin
end;

procedure TfrmMain.SetActiveDocument(docIndex: Integer);
var
  m  : TParaMetric;
begin
  InitParaMetric(m);
  if displayedIndex > -1 then
  begin
    // If we've already got some text, save the existing text to the document
    // before loading the new text
    notebookItems[displayedIndex].content := moOne.Lines.Text;
    // Save the old caret position
    notebookItems[displayedIndex].selStart := moOne.SelStart;
    notebookItems[displayedIndex].selLength := moOne.SelLength;
  end;

  // Load the new data from the selection
  {if(notebookItems[docIndex].content = nil) then
  begin
    moOne.Lines.Clear;
  end else
  begin
    moOne.Lines.Text := notebookItems[docIndex].content;
  end;          }
  moOne.Lines.Text := notebookItems[docIndex].content;
  moOne.SelStart := notebookItems[docIndex].selStart;
  moOne.SelLength := notebookItems[docIndex].selLength;

  // Change the colors and font settings
  moOne.Font := settings.editorFont;
  moOne.Font.Color := frmSettings.shEditorText.Brush.Color;
  moOne.Color := frmSettings.shEditorBackground.Brush.Color;

  pnEditorCover.Visible := false;
  moOne.Visible := true;
  // Set the active notebookItem
  displayedIndex := docIndex;
end;

procedure TfrmMain.tmrThemeTimer(Sender: TObject);
var
  i,j,k: integer;
  b,b2: TBitmap;
  im, im2: TLazIntfImage;
  c,co,c2,co2: TFPColor;
  bmp,bmp2: TBitmap;
begin

  if settings.paddingUpdate then
  begin
    if frmSettings.chkUsePercentages.Checked then
    begin
      UpdatePadding;
    end else
    begin
      //pnPaddingTop.Height := frmSettings.spPaddingTop.Value;
      //pnPaddingBottom.Height := frmSettings.spPaddingBottom.Value;
      //pnPaddingLeft.Width := frmSettings.spPaddingLeft.Value;
      //pnPaddingRight.Width := frmSettings.spPaddingRight.Value;
    end;
  end;

  if settings.editorUpdate then
  begin
    settings.editorUpdate := false;
    // Editor color
    moOne.Font := settings.editorFont;
    moOne.Font.Color := frmSettings.shEditorText.Brush.Color;
    moOne.Color := frmSettings.shEditorBackground.Brush.Color;
    {pnPaddingTop.Color := rmMain.Color;
    pnPaddingBottom.Color := rmMain.Color;
    pnPaddingLeft.Color := rmMain.Color;
    pnPaddingRight.Color := rmMain.Color;
    }

    // Settings
    frmSettings.Color := moOne.Color;
    frmSettings.shVPSep.Pen.Color := frmSettings.shNotebookIcon.Brush.Color;
    // Theme page
    frmSettings.lblTheme.Font.Color := moOne.Font.Color;
    frmSettings.lblEditorTextColor.Font.Color := moOne.Font.Color;
    frmSettings.lblEditorBackgroundColor.Font.Color := moOne.Font.Color;
    frmSettings.lblSeparator.Font.Color := moOne.Font.Color;
    frmSettings.lblNotebookTextColor.Font.Color := moOne.Font.Color;
    frmSettings.lblNotebookBackgroundColor.Font.Color := moOne.Font.Color;
    frmSettings.lblNotebookHighlightText.Font.Color := moOne.Font.Color;
    frmSettings.lblNotebookHighlight.Font.Color := moOne.Font.Color;
    frmSettings.lblNotebookIcon.Font.Color := moOne.Font.Color;
    frmSettings.lblMenuIcon.Font.Color := moOne.Font.Color;
    frmSettings.lblToolbarBackground.Font.Color := moOne.Font.Color;
    frmSettings.lblToolbarIcon.Font.Color := moOne.Font.Color;
    // Theme shapes
    frmSettings.shEditorText.Pen.Color := moOne.Font.Color;
    frmSettings.shEditorBackground.Pen.Color := moOne.Font.Color;
    frmSettings.shSeparator.Pen.Color := moOne.Font.Color;
    frmSettings.shMenuIcon.Pen.Color := moOne.Font.Color;
    // Theme copy/paste buttons
    SetButtonColors(frmSettings.btnEditorText);
    SetButtonColors(frmSettings.btnEditorBackground);
    SetButtonColors(frmSettings.btnEditorSeparator);
    SetButtonColors(frmSettings.btnEditorHighlightText);
    SetButtonColors(frmSettings.btnEditorHighlight);
    SetButtonColors(frmSettings.btnNotebookText);
    SetButtonColors(frmSettings.btnNotebookBackground);
    SetButtonColors(frmSettings.btnNotebookHighlightText);
    SetButtonColors(frmSettings.btnNotebookHighlight);
    SetButtonColors(frmSettings.btnNotebookIcon);
    SetButtonColors(frmSettings.btnMenuIcon);
    SetButtonColors(frmSettings.btnToolbarBackground);
    SetButtonColors(frmSettings.btnToolbarIcon);
    // Theme buttons HERE 2
    SetButtonColors(frmSettings.btnCreateNewTheme);
    SetButtonColors(frmSettings.btnReloadThemes);
    SetGroupColors(frmSettings.grpEditor);
    SetGroupColors(frmSettings.grpNotebook);
    SetGroupColors(frmSettings.grpMenu);
    SetGroupColors(frmSettings.grpToolbar);
  end;

  if settings.separatorUpdate then
  begin
    settings.separatorUpdate := false;
    // Pair splitter color
    psMain.Color := frmSettings.shSeparator.Brush.Color;
    psLeftSide.Color := psMain.Color;
    psRightSide.Color := psMain.Color;
  end;

  if settings.menuIconUpdate then
  begin
    settings.menuIconUpdate := false;
    // ----------  Menu icons recolor ----------
    // Remove all images before working with the image list
    imgMenuColor.Clear;
    // Grab the color and convert to TFPColor. FPColor is 16 bits, TColor is 8
    co := TColorToFPColor(frmSettings.shMenuIcon.Brush.Color);
    for k := 0 to imgMenu.Count-1 do
    begin
      // Create a temp bitmap to hold the base image
      b := TBitmap.Create;
      b.PixelFormat := pf32bit;
      // Load the base image
      imgMenu.GetBitmap(k, b);
      // Convert bitmap to intfimage to manipulate with alpha channel
      im := b.CreateIntfImage;
      // Loop through all pixels
      for i := 0 to b.Width-1 do
      begin
        for j := 0 to b.Height-1 do
        begin
          // Grab the original color to preserve the alpha channel
          c := im.Colors[i, j];
          c.red := co.red;
          c.green := co.green;
          c.blue := co.blue;
          // Write the final color
          im.Colors[i, j] := c;
        end;
      end;
      // Convert back to a bitmap
      bmp := TBitmap.Create;
      bmp.PixelFormat := pf32bit;
      bmp.LoadFromIntfImage(im);
      // Add icon to new colored imagelist
      imgMenuColor.Add(bmp, nil);
    end;
    // Update image list used by treeview
    mMain.Images := imgMenuColor;
  end;

  if settings.toolbarUpdate then
  begin
    settings.toolbarUpdate := false;
    // Toolbar color
    tbMain.Color := frmSettings.shToolbarBackground.Brush.Color;
    // TODO: No way to set SelectionColor? Look into custom draw events.
    sbMain.Color := tbMain.Color; // TODO this does not work.
    pnNotebook.Color := tbMain.Color;
    psRightSide.Color := tbMain.Color; // TODO this does not work?
    pnEditorCover.Color := tbMain.Color;
  end;

  if settings.toolbarIconUpdate then
  begin
    settings.toolbarIconUpdate := false;
    pnNotebook.Font.Color := frmSettings.shToolbarIcon.Brush.Color;
    // ----------  Toolbar icons recolor ----------
    // Remove all images before working with the image list
    imgToolbarColor.Clear;
    // Grab the color and convert to TFPColor. FPColor is 16 bits, TColor is 8
    co := TColorToFPColor(frmSettings.shToolbarIcon.Brush.Color);
    for k := 0 to imgToolbar.Count-1 do
    begin
      // Create a temp bitmap to hold the base image
      b := TBitmap.Create;
      b.PixelFormat := pf32bit;
      // Load the base image
      imgToolbar.GetBitmap(k, b);
      // Convert bitmap to intfimage to manipulate with alpha channel
      im := b.CreateIntfImage;
      // Loop through all pixels
      for i := 0 to b.Width-1 do
      begin
        for j := 0 to b.Height-1 do
        begin
          // Grab the original color to preserve the alpha channel
          c := im.Colors[i, j];
          c.red := co.red;
          c.green := co.green;
          c.blue := co.blue;
          // Write the final color
          im.Colors[i, j] := c;
        end;
      end;
      // Convert back to a bitmap
      bmp := TBitmap.Create;
      bmp.PixelFormat := pf32bit;
      bmp.LoadFromIntfImage(im);
      // Add icon to new colored imagelist
      imgToolbarColor.Add(bmp, nil);
    end;
    // Update image list used by treeview
    tbMain.Images := imgToolbarColor;
  end;

  if settings.notebookUpdate then
  begin
    settings.notebookUpdate := false;
    // Notebook color
    tvNotebook.BackgroundColor := frmSettings.shNotebookBackground.Brush.Color;
    tvNotebook.Font.Color := frmSettings.shNotebookText.Brush.Color;
    tvNotebook.TreeLineColor := tvNotebook.Font.Color;
    tvNotebook.ExpandSignColor := tvNotebook.Font.Color;
    tvNotebook.SelectionColor := frmSettings.shNotebookSelectionBackground.Brush.Color;
    tvNotebook.SelectionFontColor := frmSettings.shNotebookSelection.Brush.Color;
    // Theme page
    frmSettings.shNotebookText.Pen.Color := frmSettings.shNotebookText.Brush.Color;
    frmSettings.shNotebookBackground.Pen.Color := frmSettings.shNotebookText.Brush.Color;
    frmSettings.shNotebookSelection.Pen.Color := frmSettings.shNotebookText.Brush.Color;
    frmSettings.shNotebookSelectionBackground.Pen.Color := frmSettings.shNotebookText.Brush.Color;
    frmSettings.shNotebookIcon.Pen.Color := frmSettings.shNotebookText.Brush.Color;
    frmSettings.shToolbarBackground.Pen.Color := frmSettings.shNotebookText.Brush.Color;
    frmSettings.shToolbarIcon.Pen.Color := frmSettings.shNotebookText.Brush.Color;
    // Update Settings
    frmSettings.lvSettingsCategories.ColorBackground := tvNotebook.BackgroundColor;
    frmSettings.lvSettingsCategories.ColorFont := tvNotebook.Font.Color;
    frmSettings.lvSettingsCategories.ColorSelection := tvNotebook.Font.Color;
    frmSettings.lvSettingsCategories.ColorSelectionFont := tvNotebook.BackgroundColor;
  end;

  if settings.notebookIconUpdate then
  begin
    settings.notebookIconUpdate := false;
    // ----------  Notebook icons recolor ----------
    // Remove all images before working with the image list
    imgNotebookColor.Clear;
    // Grab the color and convert to TFPColor. FPColor is 16 bits, TColor is 8
    co := TColorToFPColor(frmSettings.shNotebookIcon.Brush.Color);
    for k := 0 to imgNotebook.Count-1 do
    begin
      // Create a temp bitmap to hold the base image
      b := TBitmap.Create;
      b.PixelFormat := pf32bit;
      // Load the base image
      imgNotebook.GetBitmap(k, b);
      // Convert bitmap to intfimage to manipulate with alpha channel
      im := b.CreateIntfImage;
      // Loop through all pixels
      for i := 0 to b.Width-1 do
      begin
        for j := 0 to b.Height-1 do
        begin
          // Grab the original color to preserve the alpha channel
          c := im.Colors[i, j];
          c.red := co.red;
          c.green := co.green;
          c.blue := co.blue;
          // Write the final color
          im.Colors[i, j] := c;
        end;
      end;
      // Convert back to a bitmap
      bmp := TBitmap.Create;
      bmp.PixelFormat := pf32bit;
      bmp.LoadFromIntfImage(im);
      // Add icon to new colored imagelist
      imgNotebookColor.Add(bmp, nil);
    end;
    // Update image list used by treeview
    tvNotebook.Images := imgNotebookColor;

    // ----------  Settings icons recolor ----------
    // Remove all images before working with the image list
    frmSettings.imgSettingsIconsColor.Clear;
    // Grab the color and convert to TFPColor. FPColor is 16 bits, TColor is 8
    co := TColorToFPColor(frmSettings.shNotebookIcon.Brush.Color);
    for k := 0 to frmSettings.imgSettingsIcons.Count-1 do
    begin
      // Create a temp bitmap to hold the base image
      b := TBitmap.Create;
      b.PixelFormat := pf32bit;
      // Load the base image
      frmSettings.imgSettingsIcons.GetBitmap(k, b);
      // Convert bitmap to intfimage to manipulate with alpha channel
      im := b.CreateIntfImage;
      // Loop through all pixels
      for i := 0 to b.Width-1 do
      begin
        for j := 0 to b.Height-1 do
        begin
          // Grab the original color to preserve the alpha channel
          c := im.Colors[i, j];
          c.red := co.red;
          c.green := co.green;
          c.blue := co.blue;
          // Write the final color
          im.Colors[i, j] := c;
        end;
      end;
      // Convert back to a bitmap
      bmp := TBitmap.Create;
      bmp.PixelFormat := pf32bit;
      bmp.LoadFromIntfImage(im);
      // Add icon to new colored imagelist
      //frmSettings.imgSettingsIconsColor.Add(bmp, nil);
    end;
    // Update image list used by treeview
    //frmSettings.lvSettingsCategories.SmallImages := frmSettings.imgSettingsIconsColor;
  end;
end;

// TODO: probably don't need this any more.
procedure TfrmMain.tvNotebookCustomDrawItem(Sender: TCustomTreeView;
  Node: TTreeNode; State: TCustomDrawState; var DefaultDraw: Boolean);
begin
  //Sender.Canvas.Font.Color := clRed;
end;

end.

