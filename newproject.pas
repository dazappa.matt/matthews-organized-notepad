unit newproject;

{$mode objfpc}{$H+}

interface

uses
  LCLType, Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, StdCtrls, resourcestrings;

type

  { TfrmNewProject }

  TfrmNewProject = class(TForm)
    btnBrowsePath: TButton;
    btnCreateProject: TButton;
    dlgBrowsePath: TSelectDirectoryDialog;
    txtProjectName: TEdit;
    txtProjectPath: TEdit;
    lblProjectName: TLabel;
    lblProjectPath: TLabel;
    procedure btnBrowsePathClick(Sender: TObject);
    procedure btnCreateProjectClick(Sender: TObject);
    procedure setCreateProject();
    procedure setSaveAs();
    procedure NewProject();
  private
    { private declarations }
  public
    { public declarations }
  end;

const
  dNewProject = 0;
  dSaveAs = 1;

var
  frmNewProject: TfrmNewProject;
  dialogOption: integer = dNewProject;

implementation

{$R *.lfm}

{ TfrmNewProject }

procedure TfrmNewProject.setCreateProject();
begin
  frmNewProject.Caption := SNewProject;
  btnCreateProject.Caption := SCreateProject;
  dialogOption := dNewProject;
end;

procedure TfrmNewProject.setSaveAs();
begin
  frmNewProject.Caption := SSaveProjectAs;
  btnCreateProject.Caption := SSaveProject;
  dialogOption := dSaveAs;
end;

procedure TfrmNewProject.btnBrowsePathClick(Sender: TObject);
begin
  if dlgBrowsePath.Execute then
  begin
    txtProjectPath.Text := dlgBrowsePath.FileName;
  end;
end;

procedure TfrmNewProject.NewProject();
var
  fullPath: string;
begin
  if txtProjectName.Text = '' then
  begin
    MessageDlg('Error', SEnterProjectName, mtWarning, [mbOK], 0);
    exit;
  end;

  if txtProjectPath.Text = '' then
  begin
    MessageDlg('Error', SSelectProjectPath, mtWarning, [mbOK], 0);
    exit;
  end;

  // Options are valid
  // Create new project files
  fullPath := txtProjectPath.Text + '/' + txtProjectName.text + '.novel';
  if DirectoryExists(fullPath) then
  begin
    // TODO overwrite confirmation
    MessageDlg('Error', SProjectFileExists, mtWarning, [mbOK], 0);
    exit;
  end;
  if CreateDir(fullPath) then
  begin
    // Write index file, project settings
  end else
  begin
    MessageDlg('Error', SErrorCreatingProject, mtWarning, [mbOK], 0);
    exit;
  end;
  frmNewProject.Hide;
end;

procedure TfrmNewProject.btnCreateProjectClick(Sender: TObject);
begin
  case dialogOption of
    dNewProject: NewProject;
    //dSaveAs: SaveProjectAs;
  end;
end;

end.

